package com.example.yevhenii.touristapp.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.yevhenii.touristapp.models.Place;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by yevhenii on 04.06.16.
 */
public class DataLoader extends AsyncTask<IListSetter, Void, Void>{

    private static final String TAG = DataLoader.class.getSimpleName();

    private String _url;
    private ProgressBar _progressBar;

    public DataLoader (String url, ProgressBar progressBar) {
        _url = url;
        _progressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        _progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected Void doInBackground(IListSetter... params) {
        getData(params[0]);
        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        _progressBar.setVisibility(View.GONE);
    }

    public void getData(final IListSetter callback) {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(_url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        Api api = builder.build().create(Api.class);
        api.getMainList().enqueue(new Callback<List<Place>>() {

            @Override
            public void onResponse(Response<List<Place>> response, Retrofit retrofit) {
                List<Place> places = response.body();


                Log.d(TAG, "Count of places: " + response.body().size());
                callback.setDataList(response.body());

            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, "Can't access or parse API data " + t.getMessage());
            }
        });
    }
}


