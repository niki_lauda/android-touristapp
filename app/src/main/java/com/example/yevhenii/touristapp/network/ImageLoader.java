package com.example.yevhenii.touristapp.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.yevhenii.touristapp.models.Place;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by yevhenii on 04.06.16.
 */
public class ImageLoader extends AsyncTask<IListSetter, Void, Void>{

    private static final String TAG = ImageLoader.class.getSimpleName();

    private String _url;
    private List<Place> _items;
    private IListSetter _callback;

    public ImageLoader(String url, List<Place> items) {
        _url = url;
        _items = items;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Void doInBackground(IListSetter... params) {
        loadImages(params[0]);
        return null;
    }
    private void loadImages(final IListSetter callback){
        for(int i = 0; i < _items.size(); i++){
            _items.get(i).image = loadSingleImage(_items.get(i)._id, i);
        }
        _callback = callback;
    }

    private Bitmap loadSingleImage(String id, int i){
        Bitmap image = null;
        URL url = null;
        try {
            url = new URL("http://tourist-fpmkp32.rhcloud.com/api/placeimg/" + id);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            Log.d(null, "ITERATION1: " + i);
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            Log.d(null, "ITERATION: " + i);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        _callback.setDataList(_items);
    }
}


