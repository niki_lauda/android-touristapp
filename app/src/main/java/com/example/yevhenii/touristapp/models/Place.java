package com.example.yevhenii.touristapp.models;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Place implements Serializable {
    @SerializedName("_id")
    public String _id;

    @SerializedName("name")
    public String name;

    @SerializedName("englishName")
    public String englishName;

    @SerializedName("description")
    public String description;

    @SerializedName("fotoName")
    public String fotoName;

    @SerializedName("address")
    public String address;

    @SerializedName("telephoneNumber")
    public String telephoneNumber;

    @SerializedName("webPage")
    public String webPage;

    @SerializedName("email")
    public String email;

    @SerializedName("country")
    public String country;

    @SerializedName("city")
    public String city;

    @SerializedName("region")
    public String region;

    @SerializedName("coordX")
    public String coordX;

    @SerializedName("coordY")
    public String coordY;

    @SerializedName("__v")
    public String __v;

    public transient Bitmap image;

    public String getShortDesc(){
        return description.split("\\.")[0] + '.';
    }
}
