package com.example.yevhenii.touristapp.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yevhenii.touristapp.R;
import com.example.yevhenii.touristapp.models.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by yevhenii on 18.06.16.
 */
public class DetailActivityEdit extends AppCompatActivity {
    private static final int DETAIL_ACTIVITY_EDIT = 1;

    private TextView _title;
    private ImageView _img;
    private TextView _desc;
    private TextView _address;
    private TextView _country;
    private TextView _city;

    private Place _place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_edit);

        _place = (Place) getIntent().getSerializableExtra("PlaceObject");
        _place.image = (Bitmap) getIntent().getParcelableExtra("Image");

        findComponents();
        fillComponents();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.activity_detail_menu_done:

                new AlertDialog.Builder(this)
                        .setTitle("Зміна туристичного місця")
                        .setMessage("Ви впевнені, що хочете змінити дане місце?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                saveEnteredData();
                                Intent intent = new Intent();
                                intent.putExtra("PlaceObjectEdited", _place);
                                setResult(Activity.RESULT_OK, intent);
                                finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_menu_delete)
                        .show();

                return true;
            case R.id.activity_detail_menu_disgard:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveEnteredData() {
        _place.name = _title.getText().toString();
        _place.description = _desc.getText().toString();
        _place.city = _city.getText().toString();
        _place.country = _country.getText().toString();
        _place.address = _address.getText().toString();
    }

    private void findComponents() {
        _title = (EditText) findViewById(R.id.activity_detail_edit_title);
        //_img = (ImageView) findViewById(R.id.activity_detail_edit_img);
        _desc = (EditText) findViewById(R.id.activity_detail_edit_desc);
        _city = (EditText) findViewById(R.id.activity_detail_edit_city);
        _country = (EditText) findViewById(R.id.activity_detail_edit_country);
        _address = (EditText) findViewById(R.id.activity_detail_edit_address);
    }

    private void fillComponents() {
        _title.setText(_place.name);
        //_img.setImageBitmap((Bitmap) getIntent().getParcelableExtra("Image"));
        //_desc.setText(Html.fromHtml("<strong>Опис:</strong>"));
        _desc.append(_place.description);
        _desc.setMovementMethod(new ScrollingMovementMethod());
        //_country.setText(Html.fromHtml("<strong>Країна:</strong>"));
        _country.append(_place.country);
        //_city.setText(Html.fromHtml("<strong>Місто:</strong>"));
        _city.append(_place.city);
        //_address.setText(Html.fromHtml("<strong>Адреса:</strong>"));
        _address.append(_place.address);

    }
}
