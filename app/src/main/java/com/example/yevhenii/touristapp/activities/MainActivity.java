package com.example.yevhenii.touristapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.yevhenii.touristapp.R;
import com.example.yevhenii.touristapp.custom.ListAdapter;
import com.example.yevhenii.touristapp.models.Place;
import com.example.yevhenii.touristapp.network.DataLoader;
import com.example.yevhenii.touristapp.network.IListSetter;
import com.example.yevhenii.touristapp.network.ImageLoader;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String _url = "http://tourist-fpmkp32.rhcloud.com/api/";
    private static final int DETAIL_ACTIVITY = 0;
    private static final int DETAIL_ACTIVITY_EDIT = 1;


    private Context _context;
    private ListView _listView;
    private ProgressBar _progressBar;
    private DataLoader _dataLoader;
    private ImageLoader _imageLoader;
    private List<Place> _items;
    private ListAdapter _adapter;
    private int _itemSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _context = this;
        initListData(this);

    }

    private void initListData (final Context context) {
        _listView = (ListView) findViewById(R.id.main_list_view);

        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(_context, DetailActivity.class);
                intent.putExtra("PlaceObject", _items.get(position));
                intent.putExtra("Image", _items.get(position).image);
                _itemSelected = position;
                startActivityForResult(intent, DETAIL_ACTIVITY);
            }
        });

        _progressBar = (ProgressBar) findViewById(R.id.progressBar);
        _dataLoader = new DataLoader(_url, _progressBar);

        _dataLoader.execute(new IListSetter() {
            @Override
            public void setDataList(List<Place> data) {
                _items = data;
                Log.d(TAG, "Count of items: " + data.size());
                _adapter = new ListAdapter(context, data, R.layout.list_view_item);
                _listView.setAdapter(_adapter);
                _imageLoader = new ImageLoader(_url, data);
                _imageLoader.execute(new IListSetter() {
                    @Override
                    public void setDataList(List<Place> data) {
                        _items = data;
                        Log.d(TAG, "Count of items: " + data.size());
                        _adapter.notifyDataSetChanged();
                    }
                });

            }});
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (DETAIL_ACTIVITY) : {
                if (resultCode == Activity.RESULT_OK) {
                    String newText = data.getStringExtra("ACTION_REMOVED");
                    if(newText.compareTo("REMOVED_SUCCESS") == 0){
                        removeFromList();
                    }
                }
                if (resultCode == DETAIL_ACTIVITY_EDIT) {
                    Place placeEdited = (Place)data.getSerializableExtra("PlaceObjectEdited");
                    Place place = _items.get(_itemSelected);
                    place.name = placeEdited.name;
                    place.address = placeEdited.address;
                    place.country = placeEdited.country;
                    place.city = placeEdited.city;
                    place.description = placeEdited.description;
                    _adapter.notifyDataSetChanged();
                }
                break;
            }
        }
    }

    private void removeFromList(){
        _items.remove(_itemSelected);
        _adapter.notifyDataSetChanged();

        Context context = getApplicationContext();
        CharSequence text = "Туристичне місце видалено!";
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
