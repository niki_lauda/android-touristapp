package com.example.yevhenii.touristapp.custom;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yevhenii.touristapp.R;
import com.example.yevhenii.touristapp.models.Place;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by yevhenii on 04.06.16.
 */
public class ListAdapter extends BaseAdapter {

    private Context _context;
    private List<Place> _items;
    private int _layoutResourceId;

    public ListAdapter (Context context, List<Place> data, int layoutResourceId) {
        _context = context;
        _items = data;
        _layoutResourceId = layoutResourceId;
    }

    @Override
    public int getCount() {
        return _items.size();
    }

    @Override
    public Object getItem(int position) {
        return _items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        Place place = _items.get(position);
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) _context).getLayoutInflater();
            convertView = inflater.inflate(_layoutResourceId, parent, false);
        }

        holder.title = (TextView) convertView.findViewById(R.id.list_view_item_title);
        holder.title.setText(place.name);
        holder.img = (ImageView) convertView.findViewById(R.id.list_view_item_img);
        holder.img.setVisibility(View.VISIBLE);
        holder.img.setImageBitmap(place.image);
        holder.desc = (TextView) convertView.findViewById(R.id.list_view_item_desc);
        holder.desc.setText(place.getShortDesc());

    return convertView;
    }

    private class Holder {
        TextView title;
        ImageView img;
        TextView desc;
    }
}
