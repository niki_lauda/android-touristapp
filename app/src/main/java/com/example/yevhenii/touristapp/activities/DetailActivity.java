package com.example.yevhenii.touristapp.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yevhenii.touristapp.R;
import com.example.yevhenii.touristapp.models.Place;
import com.example.yevhenii.touristapp.network.Api;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by yevhenii on 04.06.16.
 */
public class DetailActivity extends AppCompatActivity {
    private static final String _url = "http://tourist-fpmkp32.rhcloud.com/api/";
    private static final int DETAIL_ACTIVITY_EDIT = 1;

    private TextView _title;
    private ImageView _img;
    private TextView _desc;
    private TextView _address;
    private TextView _country;
    private TextView _city;
    private GoogleMap _map;

    private Place _place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        _place = (Place) getIntent().getSerializableExtra("PlaceObject");
        _place.image = (Bitmap) getIntent().getParcelableExtra("Image");

        findComponents();
        initMap();
        fillComponents();

    }

    private void findComponents(){
        _title = (TextView) findViewById(R.id.activity_detail_title);
        _img = (ImageView) findViewById(R.id.activity_detail_img);
        _desc = (TextView) findViewById(R.id.activity_detail_desc);
        _city = (TextView) findViewById(R.id.activity_detail_city);
        _country = (TextView) findViewById(R.id.activity_detail_country);
        _address = (TextView) findViewById(R.id.activity_detail_address);
    }

    private void fillComponents(){
        _title.setText(_place.name);
        _img.setImageBitmap((Bitmap) getIntent().getParcelableExtra("Image"));
        _desc.setText(Html.fromHtml("<strong>Опис:</strong>"));
        _desc.append(_place.description);
        _desc.setMovementMethod(new ScrollingMovementMethod());
        _country.setText(Html.fromHtml("<strong>Країна:</strong>"));
        _country.append(_place.country);
        _city.setText(Html.fromHtml("<strong>Місто:</strong>"));
        _city.append(_place.city);
        _address.setText(Html.fromHtml("<strong>Адреса:</strong>"));
        _address.append(_place.address);

    }

    private void initMap(){
        _map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        _map.moveCamera(CameraUpdateFactory.newLatLng(
                new LatLng(Double.parseDouble(_place.coordX),Double.parseDouble(_place.coordY))));
        _map.animateCamera(CameraUpdateFactory.zoomTo(10));

        _map.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(_place.coordX),Double.parseDouble(_place.coordY)))
                .title(_place.name)
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.activity_detail_menu_remove:
                showAllert();
                return true;
            case R.id.activity_detail_menu_edit:
                Intent intent = new Intent(DetailActivity.this, DetailActivityEdit.class);
                intent.putExtra("PlaceObject", _place);
                intent.putExtra("Image", _place.image);
                startActivityForResult(intent, DETAIL_ACTIVITY_EDIT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (DETAIL_ACTIVITY_EDIT) : {
                if (resultCode == Activity.RESULT_OK) {
                    Place placeEdited = (Place)data.getExtras().getSerializable("PlaceObjectEdited");
                    postApi(placeEdited);
                  break;
                }
                break;
            }
        }
    }

    @Override
    public void onStop(){
        Intent intent = new Intent();
        intent.putExtra("PlaceObjectEdited", _place);
        setResult(DETAIL_ACTIVITY_EDIT, intent);
        super.onStop();
    }

    private void postApi(final Place placeEdited) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(_url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        Api api = builder.build().create(Api.class);
        Call<ResponseBody> putRequest = api.updateItem(placeEdited._id, placeEdited);
        putRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                showToast("Туристичне місце відредаговано!");

                _place.name = placeEdited.name;
                _place.address = placeEdited.address;
                _place.country = placeEdited.country;
                _place.city = placeEdited.city;
                _place.description = placeEdited.description;
                fillComponents();

                Log.d(null, response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                showToast("Туристичне місце не вдалося відредагувати!");
                Log.d(null, t.getMessage());
            }
        });
    }


    private void showToast(String message) {
        Context context = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }


    private void showAllert(){
        new AlertDialog.Builder(this)
                .setTitle("Видалення туристичного місця")
                .setMessage("Ви впевнені, що хочете видалити дане місце?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        remove(_place._id);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_menu_delete)
                .show();
    }

    private void remove(String id){
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(_url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        Api api = builder.build().create(Api.class);
        Call<ResponseBody> deleteRequest = api.deleteItem(id);
        deleteRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                Log.d(null, response.message());

                Intent resultIntent = new Intent();
                resultIntent.putExtra("ACTION_REMOVED", "REMOVED_SUCCESS");
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }

            @Override
            public void onFailure(Throwable t) {
                //error
                Log.d(null, t.getMessage());
            }
        });
    }
}
