package com.example.yevhenii.touristapp.network;

import com.example.yevhenii.touristapp.models.Place;

import java.util.List;

/**
 * Created by yevhenii on 04.06.16.
 */
public interface IListSetter {

    void setDataList(List<Place> data);
}
