package com.example.yevhenii.touristapp.activities;

import android.app.Activity;
import android.os.Bundle;

import com.example.yevhenii.touristapp.R;

/**
 * Created by yevhenii on 18.06.16.
 */
public class FrontActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front);
    }
}
