package com.example.yevhenii.touristapp.models;

import java.util.List;

/**
 * Created by yevhenii on 04.06.16.
 */
public class ListResponse {
    public List<Place> getList() {
        return list;
    }

    public void setList(List<Place> list) {
        this.list = list;
    }

    private List<Place> list;
}
