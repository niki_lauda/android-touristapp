package com.example.yevhenii.touristapp.network;

import com.example.yevhenii.touristapp.models.Place;
import com.squareup.okhttp.ResponseBody;

import java.util.List;

import retrofit.Call;
import retrofit.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.PUT;
import retrofit.http.Path;


/**
 * Created by yevhenii on 04.06.16.
 */
public interface Api {

    @GET("place")
    Call<List<Place>> getMainList();

    @GET("place/{id}")
    Call<Place> getItem(@Path("id") int itemId);

    @DELETE("place/{id}")
    Call<ResponseBody>  deleteItem(@Path("id") String itemId);

    @PUT("place/{id}")
    Call<ResponseBody>  updateItem(@Path("id") String itemId, @Body Place place);
}
